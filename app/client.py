import pyaudio
import socket
import time

from pynput.keyboard import Key, Listener
from threading import Thread


class VoiceChatClient:
    PUSH_TO_TALK_KEY = 't'

    CONNECTION_WAIT_TIME = 1
    CHUNK_SIZE = 1024

    AUDIO_FORMAT = pyaudio.paInt16
    AUDIO_CHANNELS = 1
    AUDIO_RATE = 20000

    def __init__(self, name: str, server: str, room: str):
        self.name = name
        self.room = room
        self.server_ip, self.server_port = server.rsplit(':', 1)
        self.server_port = int(self.server_port)
        self.users_port = self.server_port + 1
        self.init_audio()
        self.talk = False
        self.run()

    def init_audio(self) -> None:
        self.audio = pyaudio.PyAudio()
        options = {
            'format': self.AUDIO_FORMAT,
            'channels': self.AUDIO_CHANNELS,
            'rate': self.AUDIO_RATE,
            'frames_per_buffer': self.CHUNK_SIZE,
        }
        self.recording_stream = self.audio.open(input=True, **options)
        self.playing_stream = self.audio.open(output=True, **options)

    def do_connect(self, server_port: int) -> socket.socket:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.server_ip, server_port))
        return sock

    def connect(self, server_port: int) -> socket.socket:
        while True:
            try:
                return self.do_connect(server_port)
            except Exception as e:
                print('Cannot connect:', str(e))
                time.sleep(self.CONNECTION_WAIT_TIME)

    def send_some_info(self):
        self.socket.sendall(self.name.encode('utf-8'))
        self.socket.sendall(b'\n')
        self.socket.sendall(self.room.encode('utf-8'))
        self.socket.sendall(b'\n')
        self.users_socket.sendall(self.room.encode('utf-8'))

    def run(self) -> None:
        self.socket = self.connect(self.server_port)
        print(f'Connected to {self.server_ip}:{self.server_port}')
        self.users_socket = self.connect(self.users_port)

        self.send_some_info()

        send_thr = Thread(target=self.send_data)
        send_thr.start()

        receive_thr = Thread(target=self.receive_data)
        receive_thr.start()

        users_thr = Thread(target=self.receive_users_list)
        users_thr.start()

    def on_press(self, key) -> None:
        try:
            self.talk = key.char == self.PUSH_TO_TALK_KEY
        except:
            pass

    def on_release(self, key) -> None:
        try:
            self.talk = self.talk and key.char != self.PUSH_TO_TALK_KEY
        except:
            pass

    def send_data(self):
        with Listener(on_press=self.on_press, on_release=self.on_release) as listener:
            while True:
                try:
                    data = self.recording_stream.read(self.CHUNK_SIZE)
                    if self.talk:
                        self.socket.sendall(data)
                except:
                    pass

    def receive_data(self):
        while True:
            try:
                data = self.socket.recv(self.CHUNK_SIZE)
                self.playing_stream.write(data)
            except:
                pass

    def receive_users_list(self):
        while True:
            try:
                data = self.users_socket.recv(self.CHUNK_SIZE)
                if data:
                    print(data.decode('utf-8'))
            except:
                pass

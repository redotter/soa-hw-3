import socket

import const


def main():
    sock = socket.socket()
    sock.connect(('', const.SERVER_PORT))

    while True:
        request = input('Enter request: ').encode('utf-8')
        sock.send(request)
        response = sock.recv(const.BYTES).decode('utf-8')
        if not response:
            break
        print('Server >', response)

    sock.close()
    print('Socket closed')


if __name__ == '__main__':
    main()

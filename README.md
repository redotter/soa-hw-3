# Voice Chat

## Hello World

A simple app to work with sockets. The client sends a string, the server returns its length.

Directory: `hello`

Run the server:
```
$ python3 server.py
Listening…
Connection accepted
Client > hello
Client > exit
Socket closed
```

Run the client:
```
$ python3 client.py
Enter request: hello
Server > len(hello) = 5
Enter request: exit
Socket closed
```


## Main app

Directory: `app`

Run the server:
```
$ ./run_server --ip 127.0.0.1 --port 9090
Successfuly bound socket on 127.0.0.1:9090
Successfuly bound socket on 127.0.0.1:9091
Running server on 127.0.0.1:9090
<kenobi> connected to room high_ground, ('127.0.0.1', 55954)
<luke> connected to room high_ground, ('127.0.0.1', 55958)
<anakin> connected to room low_ground, ('127.0.0.1', 55962)
<luke> disconnected, ('127.0.0.1', 55958)
```

Run the client:
```
$ ./run_client --server 127.0.0.1:9090 --name kenobi --room high_ground
Connected to 127.0.0.1:9090
Room <high_ground>: kenobi
Room <high_ground>: kenobi, luke
Room <high_ground>: kenobi, luke
Room <high_ground>: kenobi*, luke
Room <high_ground>: kenobi, luke
Room <high_ground>: kenobi, luke
Room <high_ground>: kenobi*
Room <high_ground>: kenobi*
Room <high_ground>: kenobi
Room <high_ground>: kenobi
```

```

$ ./run_client --server 127.0.0.1:9090 --name luke --room high_ground
Connected to 127.0.0.1:9090
Room <high_ground>: kenobi
Room <high_ground>: kenobi, luke
Room <high_ground>: kenobi, luke
Room <high_ground>: kenobi*, luke
Room <high_ground>: kenobi, luke
Room <high_ground>: kenobi, luke
<Ctrl-D>
```

```
$ ./run_client --server 127.0.0.1:9090 --name anakin --room low_ground
Connected to 127.0.0.1:9090
Room <low_ground>: anakin
Room <low_ground>: anakin*
Room <low_ground>: anakin*
Room <low_ground>: anakin
```

To speak, you need to press `t`.
If the user is speaking, an asterisk is displayed next to their name.

import socket
import time
import typing as t

from threading import Thread


class Client:
    def __init__(self, name: str, socket: socket.socket, address: str):
        self.name = name
        self.socket = socket
        self.address = address
        self.connected = True
        self.speak_time = None

    def speaking(self):
        if self.speak_time is None:
            return False
        return time.time() - self.speak_time < 1


class VoiceChatServer:
    MAX_CONNECTIONS = 10
    BINDING_WAIT_TIME = 1
    CHUNK_SIZE = 1024

    def __init__(self, ip: int, port: int):
        self.ip = ip
        self.main_port = port
        self.users_port = port + 1
        self.rooms: t.Dict[str, t.List[Client]] = {}
        self.run()

    def do_bind(self, port: int) -> socket.socket:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((self.ip, port))
        print(f'Successfuly bound socket on {self.ip}:{port}')
        return sock

    def bind(self, port: str) -> socket.socket:
        while True:
            try:
                return self.do_bind(port)
            except Exception as e:
                print('Cannot bind:', str(e))
                time.sleep(self.BINDING_WAIT_TIME)

    def listen(self, sock: socket.socket) -> None:
        sock.listen(self.MAX_CONNECTIONS)

    def accept_connections(self, sock: socket.socket, handler: t.Callable) -> None:
        while True:
            conn, addr = sock.accept()
            thr = Thread(target=handler, args=(conn, addr))
            thr.start()

    def run(self):
        main_socket = self.bind(self.main_port)
        self.listen(main_socket)

        users_socket = self.bind(self.users_port)
        self.listen(users_socket)
        print(f'Running server on {self.ip}:{self.main_port}')

        accept_thr = Thread(
            target=self.accept_connections,
            args=(main_socket, self.handle_client)
        )
        accept_thr.start()

        users_thr = Thread(
            target=self.accept_connections,
            args=(users_socket, self.handle_users_list)
        )
        users_thr.start()

    def handle_client(self, conn: socket.socket, addr: str) -> None:
        data = conn.recv(self.CHUNK_SIZE).decode('utf-8')
        name, room, data = data.split('\n')
        print(f'<{name}> connected to room {room}, {addr}')
        if room not in self.rooms:
            self.rooms[room] = []
        client = Client(name, conn, addr)
        self.rooms[room].append(client)

        while True:
            try:
                if data:
                    client.speak_time = time.time()
                    self.broadcast(room, data, conn)
                data = conn.recv(self.CHUNK_SIZE)
            except socket.error:
                self.disconnect_client(conn)
                break

    def list_users(self, room: str) -> str:
        users = []
        for client in self.rooms[room]:
            if client.connected:
                name = client.name
                if client.speaking():
                    name += '*'
                users.append(name)
        return f'Room <{room}>: {", ".join(users)}'

    def handle_users_list(self, conn: socket.socket, addr: str) -> None:
        room = conn.recv(self.CHUNK_SIZE).decode('utf-8')
        while room not in self.rooms:
            time.sleep(0.5)
            continue
        while True:
            self.rooms[room] = [client for client in self.rooms[room] if client.connected]
            try:
                conn.sendall(self.list_users(room).encode('utf-8'))
                time.sleep(1)
            except socket.error:
                break

    def disconnect_client(self, sock: socket.socket) -> None:
        for room in self.rooms.keys():
            for client in self.rooms[room]:
                if client.socket == sock:
                    client.connected = False
                    print(f'<{client.name}> disconnected, {client.address}')

    def broadcast(self, room: str, data: bytes, sock: socket.socket):
        for client in self.rooms[room]:
            if len(self.rooms[room]) > 1 and client.socket == sock:
                continue
            if client.connected:
                try:
                    client.socket.sendall(data)
                except socket.error:
                    self.disconnect_client(client.socket)

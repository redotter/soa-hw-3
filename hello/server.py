import socket

import const


def main():
    sock = socket.socket()
    sock.bind(('', const.SERVER_PORT))

    sock.listen(1)
    print('Listening…')

    conn, addr = sock.accept()
    print('Connection accepted')

    while True:
        request = conn.recv(const.BYTES).decode('utf-8')
        print('Client >', request)
        if request == 'exit':
            break
        response = f'len({request}) = {len(request)}'
        conn.send(response.encode('utf-8'))

    conn.close()
    print('Socket closed')


if __name__ == '__main__':
    main()
